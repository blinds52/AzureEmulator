#region

using System;
using Azure.Messages;
using Azure.Util;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.Users;

#endregion

namespace Azure.HabboHotel.Rooms
{
    /// <summary>
    /// Class Chatlog.
    /// </summary>
    internal class Chatlog
    {
        /// <summary>
        /// The user identifier
        /// </summary>
        internal uint UserId;

        /// <summary>
        /// The message
        /// </summary>
        internal string Message;

        /// <summary>
        /// The timestamp
        /// </summary>
        internal DateTime TimeStamp;

        /// <summary>
        /// The is saved
        /// </summary>
        internal bool IsSaved;

        /// <summary>
        /// Initializes a new instance of the <see cref="Chatlog"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="time">The time.</param>
        /// <param name="globalMessage"></param>
        /// <param name="fromDatabase">if set to <c>true</c> [from database].</param>
        internal Chatlog(uint user, string msg, DateTime time, bool fromDatabase, uint roomId)
        {
            UserId = user;
            Message = msg;
            TimeStamp = time;
            IsSaved = fromDatabase;
            if (!IsSaved) Save(roomId);
        }

        /// <summary>
        /// Saves the specified room identifier.
        /// </summary>
        /// <param name="queryChunk"></param>
        /// <param name="id">Auto increment</param>
        internal void Save(uint RoomId)
        {
            using (IQueryAdapter queryChunk = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                queryChunk.SetQuery("INSERT INTO users_chatlogs (user_id, room_id, timestamp, message) VALUES (@user, @room, @time, @message)");
                queryChunk.AddParameter("user", UserId);
                queryChunk.AddParameter("room", RoomId);
                queryChunk.AddParameter("time", AzureEmulator.DateTimeToUnix(TimeStamp));
                queryChunk.AddParameter("message", Message);
                queryChunk.RunQuery();
            }
            IsSaved = true;
        }

        internal void Serialize(ref ServerMessage message)
        {
            Habbo habbo = AzureEmulator.GetHabboById(UserId);
            message.AppendInteger(AzureEmulator.DifferenceInMilliSeconds(TimeStamp, DateTime.Now));
            message.AppendInteger(UserId);
            message.AppendString(habbo == null ? "*User not found*" : habbo.UserName);
            message.AppendString(Message);
            message.AppendBool(true);
        }
    }
}