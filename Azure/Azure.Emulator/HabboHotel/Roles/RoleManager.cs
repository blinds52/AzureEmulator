#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;

#endregion

namespace Azure.HabboHotel.Roles
{
    /// <summary>
    /// Class RoleManager.
    /// </summary>
    internal class RoleManager
    {
        public Dictionary<string, int> CommandsList = new Dictionary<string, int>();
        public Dictionary<string, int> PetCommandsList = new Dictionary<string, int>();
        public Dictionary<uint, string> RankBadge = new Dictionary<uint, string>();
        private Dictionary<uint, int> RankFlood = new Dictionary<uint, int>();
        private Dictionary<uint, List<string>> RankPermissions = new Dictionary<uint, List<string>>();
        private Dictionary<uint, List<string>> UserPermissions = new Dictionary<uint, List<string>>();

        public void ClearPermissions()
        {
            this.RankBadge.Clear();
            this.UserPermissions.Clear();
            this.RankPermissions.Clear();
            this.RankFlood.Clear();
        }

        public bool ContainsRank(uint Rank)
        {
            return this.RankPermissions.ContainsKey(Rank);
        }

        public bool ContainsUser(uint UserID)
        {
            return this.UserPermissions.ContainsKey(UserID);
        }

        public int FloodTime(uint RankId)
        {
            return this.RankFlood[RankId];
        }

        public List<string> GetRightsForHabbo(uint UserID, uint Rank)
        {
            List<string> list = new List<string>();
            if (this.ContainsUser(UserID))
            {
                return this.UserPermissions[UserID];
            }
            return this.RankPermissions[Rank];
        }

        public void LoadRights(IQueryAdapter dbClient)
        {
            this.ClearPermissions();
            dbClient.SetQuery("SELECT * FROM ranks ORDER BY id ASC;");
            DataTable table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    this.RankBadge.Add((uint)row["id"], row["badgeid"].ToString());
                }
            }
            dbClient.SetQuery("SELECT * FROM permissions_ranks ORDER BY rank ASC;");
            table = dbClient.GetTable();
            if (table != null)
            {
                foreach (DataRow row3 in table.Rows)
                {
                    
                }
                foreach (DataRow row in table.Rows)
                {
                    this.RankFlood.Add((uint)row["rank"], (int)row["floodtime"]);

                    List<string> list = new List<string>();
                    if (AzureEmulator.EnumToBool(row["support_tool"].ToString()))
                    {
                        list.Add("support_tool");
                    }
                    if (AzureEmulator.EnumToBool(row["ambassadeur"].ToString()))
                    {
                        list.Add("ambassadeur");
                    }
                    if (AzureEmulator.EnumToBool(row["moderator"].ToString()))
                    {
                        list.Add("moderator");
                    }
                    if (AzureEmulator.EnumToBool(row["manager"].ToString()))
                    {
                        list.Add("manager");
                    }
                    if (AzureEmulator.EnumToBool(row["admin"].ToString()))
                    {
                        list.Add("admin");
                    }
                    if (AzureEmulator.EnumToBool(row["staff_chat"].ToString()))
                    {
                        list.Add("staff_chat");
                    }
                    if (AzureEmulator.EnumToBool(row["user_is_staff"].ToString()))
                    {
                        list.Add("user_is_staff");
                    }
                    if (AzureEmulator.EnumToBool(row["user_not_kickable"].ToString()))
                    {
                        list.Add("user_not_kickable");
                    }
                    if (AzureEmulator.EnumToBool(row["user_can_read_whisper"].ToString()))
                    {
                        list.Add("user_can_read_whisper");
                    }
                    if (AzureEmulator.EnumToBool(row["user_can_change_name"].ToString()))
                    {
                        list.Add("user_can_change_name");
                    }
                    if (AzureEmulator.EnumToBool(row["user_enter_full_rooms"].ToString()))
                    {
                        list.Add("user_enter_full_rooms");
                    }
                    if (AzureEmulator.EnumToBool(row["user_enter_any_room"].ToString()))
                    {
                        list.Add("user_enter_any_room");
                    }
                    if (AzureEmulator.EnumToBool(row["user_control_any_room"].ToString()))
                    {
                        list.Add("user_control_any_room");
                    }
                    if (AzureEmulator.EnumToBool(row["user_room_staff_pick"].ToString()))
                    {
                        list.Add("user_room_staff_pick");
                    }
                    if (AzureEmulator.EnumToBool(row["cmd_global_refresh_permissions"].ToString()))
                    {
                        list.Add("cmd_global_refresh_permissions");
                    }
                    if (AzureEmulator.EnumToBool(row["cmd_user_can_give_currency"].ToString()))
                    {
                        list.Add("cmd_user_can_give_currency");
                    }
                    if (AzureEmulator.EnumToBool(row["modtool_can_read_chatlog"].ToString()))
                    {
                        list.Add("modtool_can_read_chatlog");
                    }
                    if (AzureEmulator.EnumToBool(row["modtool_can_send_alert"].ToString()))
                    {
                        list.Add("modtool_can_send_alert");
                    }
                    if (AzureEmulator.EnumToBool(row["modtool_can_ban_user"].ToString()))
                    {
                        list.Add("modtool_can_ban_user");
                    }
                    if (AzureEmulator.EnumToBool(row["modtool_can_kick_user"].ToString()))
                    {
                        list.Add("modtool_can_kick_user");
                    }

                    this.RankPermissions.Add((uint)row["rank"], list);
                }
            }
        }

        public int RankCount()
        {
            return this.RankBadge.Count;
        }

        public bool RankHasRight(uint RankId, string Role)
        {
            if (!this.ContainsRank(RankId))
            {
                return false;
            }
            List<string> list = this.RankPermissions[RankId];
            return list.Contains(Role);
        }

        public string RanksBadge(uint Rank)
        {
            return this.RankBadge[Rank];
        }

        public bool UserHasPermission(uint UserID, string Role)
        {
            if (!this.ContainsUser(UserID))
            {
                return false;
            }
            List<string> list = this.UserPermissions[UserID];
            return list.Contains(Role);
        }

        public bool UserHasPersonalPermissions(uint UserID)
        {
            return this.ContainsUser(UserID);
        }

    }
}