﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MuteBots. This class cannot be inherited.
    /// </summary>
    internal sealed class MuteBots : Command
    {
        public MuteBots()
        {
            MinParams = 0;
            Description = "Mute all bots in the current room.";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("user_is_staff") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id || session.GetHabbo().CurrentRoom.CheckRights(session);
        }

        public override void Execute(GameClient session, string[] pms)
        {
            var room = session.GetHabbo().CurrentRoom;
            room.MutedBots = !room.MutedBots;
            session.SendNotif(TextManager.GetText("user_room_mute_bots"));
        }
    }
}