﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class MassCredits. This class cannot be inherited.
    /// </summary>
    internal sealed class MassCredits : Command
    {
        public MassCredits()
        {
            MinParams = -1;
            Description = "Hotel credits";
            Usage = "[credits]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_user_can_give_currency");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            int amount;
            if (!int.TryParse(pms[0], out amount))
            {
                session.SendNotif(TextManager.GetText("enter_numbers"));
                return;
            }
            foreach (GameClient client in AzureEmulator.GetGame().GetClientManager().Clients.Values)
            {
                if (client == null || client.GetHabbo() == null) continue;
                var habbo = client.GetHabbo();
                client.GetHabbo().Credits += amount;
                client.GetHabbo().UpdateCreditsBalance();
                client.SendNotif(TextManager.GetText("command_mass_credits_one_give") + amount + (TextManager.GetText("command_mass_credits_two_give")));
            }
        }
    }
}