﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HandItem. This class cannot be inherited.
    /// </summary>
    internal sealed class HandItem : Command
    {
        public HandItem()
        {
            MinParams = 1;
            Description = "Carry handitem.";
            Usage = "[id]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().VIP || session.GetHabbo().Rank > 1;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort itemId;
            if (!ushort.TryParse(pms[0], out itemId))
            {
                return;
            }

            var user = session.GetHabbo()
                    .CurrentRoom.GetRoomUserManager()
                    .GetRoomUserByHabbo(session.GetHabbo().UserName);

            if (user.RidingHorse)
            {
                session.SendWhisper(TextManager.GetText("horse_handitem_error"));
                return;
            }

            if (user.IsLyingDown)
            {
                return;
            }

            user.CarryItem(itemId);
        }
    }
}