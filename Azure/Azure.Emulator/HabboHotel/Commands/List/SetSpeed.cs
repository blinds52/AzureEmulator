﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class SetSpeed. This class cannot be inherited.
    /// </summary>
    internal sealed class SetSpeed : Command
    {
        public SetSpeed()
        {
            MinParams = 1;
            Description = "Set roller speed.";
            Usage = "[speed]";
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("user_is_staff") || session.GetHabbo().CurrentRoom.RoomData.OwnerId == session.GetHabbo().Id || session.GetHabbo().CurrentRoom.CheckRights(session);
        }

        public override void Execute(GameClient session, string[] pms)
        {
            uint speed;
            if (uint.TryParse(pms[0], out speed)) session.GetHabbo().CurrentRoom.GetRoomItemHandler().SetSpeed(speed);
            else session.SendWhisper(TextManager.GetText("command_setspeed_error_numbers"));
        }
    }
}