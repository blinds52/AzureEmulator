﻿#region

using System;
using Azure.Configuration;
using Azure.Database.Manager.Database.Session_Details.Interfaces;
using Azure.HabboHotel.GameClients;
using Azure.Security;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshFilter : Command
    {
        public RefreshFilter()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_bannedhotels_desc"); // TODO Description
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            BobbaFilter.InitSwearWord();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}