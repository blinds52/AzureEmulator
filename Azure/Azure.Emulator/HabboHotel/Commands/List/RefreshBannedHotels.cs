﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.Security;
using Azure.Security.BlackWords;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshBannedHotels. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshBannedHotels : Command
    {
        public RefreshBannedHotels()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_bannedhotels_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            Filter.Reload();
            BlackWordsManager.Reload();

            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}