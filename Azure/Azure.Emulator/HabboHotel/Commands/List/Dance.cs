﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;
using Azure.Messages;
using Azure.Messages.Parsers;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class Dance. This class cannot be inherited.
    /// </summary>
    internal sealed class Dance : Command
    {
        public Dance()
        {
            MinParams = 1;
            Description = "User can choose a dance move.";
            Usage = "[1/4]";
        }

        public override bool CanExecute(GameClient session)
        {
            return true;
        }

        public override void Execute(GameClient session, string[] pms)
        {
            ushort result;
            ushort.TryParse(pms[0], out result);

            if (result > 4)
            {
                session.SendWhisper(TextManager.GetText("command_dance_false"));
                result = 0;
            }
            var message = new ServerMessage();
            message.Init(LibraryParser.OutgoingRequest("DanceStatusMessageComposer"));
            message.AppendInteger(session.CurrentRoomUserId);
            message.AppendInteger(result);
            session.GetHabbo().CurrentRoom.SendMessage(message);
        }
    }
}