﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshRanks. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshRanks : Command
    {
        public RefreshRanks()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_ranks_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            using (var adapter = AzureEmulator.GetDatabaseManager().GetQueryReactor())
            {
                AzureEmulator.GetGame().GetRoleManager().LoadRights(adapter);
            }
            session.SendNotif(TextManager.GetText("command_refresh_ranks"));
        }
    }
}