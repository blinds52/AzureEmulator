﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class RefreshItems. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshItems : Command
    {
        public RefreshItems()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_items_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            FurniDataParser.SetCache();
            AzureEmulator.GetGame().Reloaditems();
            FurniDataParser.Clear();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}