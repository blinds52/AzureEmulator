﻿#region

using System;
using Azure.Configuration;
using Azure.HabboHotel.GameClients;

#endregion

namespace Azure.HabboHotel.Commands.List
{
    /// <summary>
    /// Class HotelAlert. This class cannot be inherited.
    /// </summary>
    internal sealed class RefreshPromos : Command
    {
        public RefreshPromos()
        {
            MinParams = 0;
            Description = TextManager.GetText("cmd_update_promo_desc");
        }

        public override bool CanExecute(GameClient session)
        {
            return session.GetHabbo().HasFuse("cmd_global_refresh_permissions");
        }

        public override void Execute(GameClient session, string[] pms)
        {
            AzureEmulator.GetGame().GetHotelView().RefreshPromoList();
            session.SendWhisper(TextManager.GetText("cmd_succesfully"));
        }
    }
}