﻿namespace Azure.Security.BlackWords
{
    /// <summary>
    /// Enum BlackWordType
    /// </summary>
    internal enum BlackWordType
    {
        /// <summary>
        /// The hotel
        /// </summary>
        Hotel,

        /// <summary>
        /// The insult
        /// </summary>
        Insult,

        /// <summary>
        /// All
        /// </summary>
        All
    }

}
